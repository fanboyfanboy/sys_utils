# To the extent possible under law, the author(s) have dedicated all 
# copyright and related and neighboring rights to this software to the 
# public domain worldwide. This software is distributed without any warranty. 
# You should have received a copy of the CC0 Public Domain Dedication along 
# with this software. 
# If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 

# base-files version 4.2-4

# ~/.bash_profile: executed by bash(1) for login shells.

# The latest version as installed by the Cygwin Setup program can
# always be found at /etc/defaults/etc/skel/.bash_profile

# Modifying /etc/skel/.bash_profile directly will prevent
# setup from updating it.

# The copy in your home directory (~/.bash_profile) is yours, please
# feel free to customise it to create a shell
# environment to your liking.  If you feel a change
# would be benifitial to all, please feel free to send
# a patch to the cygwin mailing list.

# User dependent .bash_profile file

# source the users bashrc if it exists
if [ -f "${HOME}/.bashrc" ] ; then
  source "${HOME}/.bashrc"
fi

# Set PATH so it includes user's private bin if it exists
# if [ -d "${HOME}/bin" ] ; then
#   PATH="${HOME}/bin:${PATH}"
# fi

# Set MANPATH so it includes users' private man if it exists
# if [ -d "${HOME}/man" ]; then
#   MANPATH="${HOME}/man:${MANPATH}"
# fi

# Set INFOPATH so it includes users' private info if it exists
# if [ -d "${HOME}/info" ]; then
#   INFOPATH="${HOME}/info:${INFOPATH}"
# fi


# --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Description: This file contains all of my BASH configurations and aliases
#     For support: Twitter @fanboyfanboydev | Email fanboyfanboydev@hotmail.com
#     Licensed under MIT - essentially use whatever/however you want. Just don't screw me over if you screw something up ;)
# 
# Sections:
# 1. Environment Configuration
# 	1.1 MacOS Profile
# 	1.2 Linux Profile
# 2. Make Terminal Great Again (aliases of all sorts)
# 	2.1 Git
# 	2.2 Directory
# 	2.3 Networking
# 	2.4 Apache
#	2.5 Apt-Get
#	2.6 Ghost 
#	2.7 Other
# 	
#          ****FWIW: Terminal > Linux BASH > Powershell > Git Bash >  Windows98 > cmd prompt****
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# NOTE: Commented out lines mean that those commands are likely server-only and not needed for workstation profile.
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------


# -------------------------------------------------------------------------
#  2. MAKE TERMINAL GREAT AGAIN
# -------------------------------------------------------------------------

# 2.1 -- GIT STUFF
alias gPull='git pull'														# Git push
alias gPush='git push'														# Git push
alias gPUsh='git push'														# Fuck it
alias gPUll='git pull'														# Tired of this shit
alias gnb='git fetch && git checkout'										# Git checkout branch (if already created remotely)
alias gdb='git branch --delete'												# Git delete branch (locally -- cannot be on branch)
alias gca='git commit -a -m'												# Git commit add all files + commit them w/ message
alias gs='git status'														# Git status
alias gb='git branch'														# Git branch
alias gba='git branch --all'												# Git branch show all
alias gd='git diff'															# Git diff
alias gcob='git checkout' 													# Git checkout ${branch_name}
alias gpod='git pull origin develop'										# Git pull origin develop 
alias gcoa='git checkout -- .'												# Git checkout all (remove all files)

# 2.2 -- DIRECTORY STUFF
alias ~='cd ~/'																# Go home
alias ..='cd ../'															# Go back 1 directory levels
alias ...='cd ../../'														# Go back 2 directory levels
alias .3='cd ../../../'														# Go back 3 directory levels
alias .4='cd ../../../../'													# Go back 4 directory levels
alias .5='cd ../../../../../'												# Go back 5 directory levels
alias .6='cd ../../../../../../'											# Go back 6 directory levels
alias c='clear'																# Clear
alias cls='clear'															# Clear (thanks powershell)
alias cDev='cd ~/dev'														# Shortcut to ~/dev
alias cTest='cd ~/dev/cpq-test'												# Shortcut to ~/dev/cpq-test
alias cpq='cd ~/dev/cpq'													# Shortcut to ~/dev/cpq
alias cSandbox='cd ~/dev/sandbox'											# Shortcut to ~/dev/sandbox
alias cAuto='cd ~/dev/cpq-test/testauto'									# Shortcut to ~/dev/cpq-test/testauto


# Useful Things
# JMeter source: http://jmeter.apache.org/usermanual/get-started.html#override
alias run_jtest="java -jar `cygpath -m /cygdrive/c/jmeter/bin/ApacheJMeter.jar` -n -t $1" 											# Run Jmeter test in non GUI mode
																			# -n: Sets non-gui=true
																			# -t: Name of .jmx file that contains test plan (easiest if inside the directory holding the test (.jmx) file)
alias client_clean='java -jar `cygpath -m /cygdrive/c/jmeter/bin/ApacheJMeter.jar` -n -t C:/cygwin64/home/cothrond/dev/cpq-test/testauto/rs/maintenance/rs_client_clean_MANUAL.jmx -l result.jtl'																		
alias view_results='cygstart result.jtl'
# 2.3 NETWORKING STUFF
function ipInfo() {															# Displays public-facing IP + geolocation 
	if [ ! -z $@ ]
	 then																	# 	if (arg == null) { my_IP } else { input_IP }
		curl https://ipinfo.io/$@/json
		echo
	else
		curl https://ipinfo.io/json
		echo
	fi
}																			

alias linuxHardware='hwinfo' 												# List linux system information

# 2.7 OTHER STUFF
alias editProfile="cygstart ~/.bash_profile"								# Open ~/.bash_profile in Sublime Text editor
alias editHosts='sudo nano /etc/hosts'										# edit hosts file
alias fucking='sudo'														# Explains it's fucking self