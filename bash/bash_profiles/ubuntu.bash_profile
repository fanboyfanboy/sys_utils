# --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Description: This file contains all of my BASH configurations and aliases
#     For support: Twitter @fanboyfanboydev | Email: support@fanboyfanboy.com
#
# Fully Licensed Under MIT. 
#
#											  FWIW: MacOS Terminal > Linux Terminal > PowerShell > GitBash >  WindowsME > cmd prompt
# Section Headings:
# 1. System Environment Configuration
# 	1.1 MacOS Profile
# 	1.2 Linux Profile
# 2. Make Terminal Great Again
# 	2.1 Git
# 	2.2 Directory
# 	2.3 Networking
# 	2.4 Apache
#	2.5 Apt-Get
#	2.6 Ghost 
#	2.7 System
# 3. Custom Functions
# 	3.1 Networking
# 		3.1.1: ip_info()
#		3.1.2: ip_info($1)
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------



# -------------------------------------------------------------------------
#  1. SYSTEM ENVIRONMENT CONFIGURATION
# -------------------------------------------------------------------------

# 1.1 -- Linux Profile
export JAVA_HOME='/usr/lib/jvm/jre-1.8.0-openjdk'											# Export JAVA_HOME
export JRE_HOME='/usr/lib/jvm/java-8-openjdk-amd64/jre'									# Export JRE_HOME
export PATH=$JAVA_HOME:$JRE_HOME:$PATH 													# Export PATH



# -------------------------------------------------------------------------
#  2. MAKE TERMINAL GREAT AGAIN
# -------------------------------------------------------------------------

# 2.1 -- GIT
alias gpull='git pull'																	# push | ying
alias gpush='git push'																	# pull | yang
alias gnb='git fetch && git checkout'											# go fetch && checkout ${branch_name}
alias gdb='git branch --delete'												# delete local ${branch_name} 
alias gdrb='git push origin --delete'									# delete remote ${branch_name} (e.g. on Bitbucket)
alias gca='git commit -a -m'													# commit --add-all-files --message ${message_text}
alias gs='git status'																# stats forgot his pants, bitch
alias gb='git branch'																# branches late for class, bitch!
alias gba='git branch --all'														# now this is how you dreammm, bitch
alias gd='git diff'																	# diff me brotha 
alias gc='git checkout' 															# daaaaamn Daniel, checkout ${branch_name} back at it again with the white vans! 
alias gf='git fetch'																	# woof?
alias gcb='git checkout -b'												# damn bro, checkout ${branch_name} && lookin sexy enough to --track

# @self: You wasted 55 minutes adding comments to 11 lines, comprised of alias' which are self-explanatory. Get ur shit together u high ass
# @future_self: Help Us.

# 2.2 -- DIRECTORY
alias ~='cd ~/'																# Home Sweet Home
alias ..='cd ../'															# -1 dir
alias ...='cd ../../'														# -2 dirs
alias .3='cd ../../../'														# -3 dirs
alias .4='cd ../../../../'													# -4 dirs
alias .5='cd ../../../../../'												# -5 dirs
alias .6='cd ../../../../../../'											# -6 dirs
alias c='clear'																# Clear
alias cls='clear'															# Clear ... thanks PS
alias cw='cd /var/www/'													# cd www
alias cghost='cd /var/www/ghost'											# cd ghost

# 2.4 -- APACHE
alias csites_available='cd /etc/apache2/sites-available'					# cd sites-available
alias aprestart='sudo service apache2 restart'								# Restart apache2
alias apreload='sudo service apache2 reload' 								# Reload apache2

# 2.5 -- APT
alias agupdate='sudo apt-get update'										# apt-get update
alias agupgrade='sudo apt-get upgrade'									# apt-get upgrade
alias agauto='sudo apt autoremove'									# apt autoremove
alias agdist='sudo apt-get dist-upgrade'							# apt-get dist-upgrade

# 2.6 -- GHOST
alias gstart='sudo ghost start'										# Start Ghost
alias gstop='sudo ghost stop'											# Stop Ghost
alias grestart='sudo ghost restart'									# Restart Ghost
alias gstatus='ghost status'											# Ghost Status

# 2.7 -- SYSTEM
alias lininfo='hwinfo' 																		# List linux system information
alias lsl='ls -laht'																					# Preferred use of `ls`
alias nanoprofie="sudo nano  ~/.bash_profile"												# Open ~/.bash_profile in Sublime Text editor
alias nanohosts='sudo nano /etc/hosts'														# edit hosts file
alias cplinprofile='cp ~/.bash_profile ~/dev/utils/bash/bash_profiles/ubuntu.bash_profile'						# Copy linux profile to dir + rename	
alias fucking='sudo'																				# `fucking reboot` -- thank me later

# -------------------------------------------------------------------------
#  3. Custom Functions
# -------------------------------------------------------------------------

# 3.1 -- Networking

# 3.1.1: ip_info()
#	Obtains public IP information
#
# @return detailed IP info + geolocation 
#
# 3.1.2: ip_info($1)
# $arg1: IPv4/IPv6 address
# @return detailed IP info + geolocation for given IP (note: returns false info if user has VPN enabled)
function ip_info() {
	if [ ! -z $@ ]
	 then
		curl https://ipinfo.io/$@/json
		echo
	else
		curl https://ipinfo.io/json
		echo
	fi
}

# 3.2 -- System

# 3.1.1: syncr()
#	My preferred use of `rsync`
#
# @return detailed IP info + geolocation 
#
# 3.1.2: ip_info($1)
# $1: source directory
# $1: target directory
function syncr() {
	rsync -r --progress $1 $2
	echo
}