Import-Module PsGet
Import-Module posh-git

#custom settings
set-location C:\cygwin64\home\cothrond\dev\cpq-test # sets default directory
$shell = $Host.UI.rawUI
$shell.WindowTitle="FanboyFanboy" #Title 
$size = $Shell.WindowSize
$size.width=120
$size.height=50
$shell.WindowSize = $size
$size = $Shell.BufferSize
$size.width =100
$size.height=5000
$shell.BackgroundColor = "Black"
$shell.ForegroundColor = "Green" #everything else should be clear

#Custom alias & functions

## GIT ##
function get-gitStatus { git status }
function get-gitFetch { git fetch }
function set-gitCommitAll { git commit -a -m $args }
function set-gitAddAll {git add --all }
function set-gitReset { git reset }
function get-gitDiff { git diff }
function get-gitPullOD { git pull origin develop }
function set-gitCheckoutAll { git checkout -- . }
function set-gitPush { git push }
function get-gitPull {git pull}
function set-gitCheckoutBranch { git checkout $args }
function get-getNewBranch { git fetch; git checkout $args }
function get-gitBranchAll { git branch --all }
function get-fileHash {get-filehash -algorithm SHA521 $args | format-list}

## FILE / TASK / PROCESS RESETS ##
function set-revertTestNG { git checkout testauto_ui/cpq/testNG.xml }
function set-killChrome { taskkill /f /im chromedriver.exe }
function set-gitDeleteBranch { git branch -D $args}
function set-revertClientClean {git checkout testauto/rs/maintenance/rs_client_clean_MANUAL.jmx}

## OTHER HELPERS ##
function set-shortClear {cls}
function set-goHome {cd ~}

new-item alias:gs -value get-gitStatus
new-item alias:gca -value set-gitCommitAll
new-item alias:gr -value set-gitReset
new-item alias:gf -value get-gitFetch
new-item alias:gaa -value set-gitAddAll
new-item alias:gd -value get-gitDiff
new-item alias:gpod -value get-gitPullOD
new-item alias:gcoa -value set-gitCheckoutAll
new-item alias:gPush -value set-gitPush
new-item alias:gPull -value get-gitPull
new-item alias:gcob -value set-gitCheckoutBranch
new-item alias:kc -value set-killChrome
new-item alias:rtng -value set-revertTestNG
new-item alias:gnb -value get-getNewBranch
new-item alias:gba -value get-gitBranchAll
new-item alias:gdb -value set-gitDeleteBranch
new-item alias:c -value set-shortClear
new-item alias:rcc -value set-revertClientClean
new-item alias:~ -value set-goHome
new-item alias:sha512 -value get-fileHash

# Clear screan at the end
cls